# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Import inventory adjust line',
    'version': '2.0',
    'category': 'stock',
    'complexity': 'easy',
    'author':'virzoteck software and solutions(virzoteck@gmail.com)',
    'website': '',
    'description': """Import Inventory adjust line easily from excel/csv to odoo""",
    'depends': ['stock', 'product'],
    'data': [
        'wizard/upload_inventory_line.xml',
        'views/stock_inventory.xml'

    ],

    'installable': True,
    'auto_install': True,
    'license': 'LGPL-3',
    "price": 19,
    "currency": "EUR"
}
