# -*- coding: utf-8 -*-

import base64

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from xlrd import open_workbook


class UploadInventoryLine(models.TransientModel):
    _name = "upload.inventory.line"
    _description = "Upload Lot Number"

    def _default_retrive_sample_file_excel(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        file_link = base_url + '/import_inventory_adjustment_line/static/description/sample.excel'
        return file_link

    def _default_retrive_sample_file_csv(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        file_link = base_url + '/import_inventory_adjustment_line/static/description/sample.csv'
        return file_link

    file_data = fields.Binary(string='Import File', required=True)
    line_seprater = fields.Char(string='Line Seprater', required=True, default="""\\n""")
    err = fields.Text(string='err')
    sample_file_link_excel = fields.Char("Download sample File for excel Format",
                                         default=_default_retrive_sample_file_excel, readonly=True)
    sample_file_link_csv = fields.Char("Download sample File for CSV Format", default=_default_retrive_sample_file_csv,
                                       readonly=True)
    file_type = fields.Selection([('xls', 'Excel File'), ('csv', 'CSV')], 'File Format', required=True, default='xls')

    @api.multi
    def upload_inventory(self):
        try:
            if self.file_type == 'xls':
                self.upload_inventory_line_by_excel()
            else:
                self.upload_inventory_line_by_csv()
        except Exception as e:
            raise ValidationError(_("ERROR %s") % (e,))
        return True

    @api.multi
    def upload_inventory_line_by_excel(self):
        lot_obj = self.env['stock.production.lot']
        product_obj = self.env['product.product']
        inventory_obj = self.env['stock.inventory']
        inventory_line_obj = self.env['stock.inventory.line']
        product_uom_obj = self.env['product.uom']

        data_file = base64.b64decode(self.file_data)
        wb = open_workbook(file_contents=data_file)
        for s in wb.sheets():
            for row in range(s.nrows):
                if row == 0:
                    continue
                product_code = (s.cell(row, 0).value)
                product_qty = (s.cell(row, 1).value)
                product_uom = (s.cell(row, 2).value)

                product_id = product_obj.search([('default_code', '=', product_code)])
                if not len(product_id):
                    raise ValidationError(_("No product code %s found in database at row %s") % (product_code,row,))

                product_uom_id = product_uom_obj.search([('name', '=', product_uom)])
                if not len(product_uom_id):
                    raise ValidationError(_(
                        "No Product UOM  found in database,UOM in file should be equal to unit of measure field, at row %s") % (
                                              row,))

                stock_inventory_data = inventory_obj.browse(self.env.context['active_id'])
                location_id = stock_inventory_data.location_id.id
                inventory_line_obj.create(
                    {'inventory_id': self.env.context['active_id'], 'product_id': product_id[0].id,
                     'location_id': location_id, 'product_qty': product_qty, 'product_uom_id': product_uom_id[0].id})
        #                self.env.cr.commit()
        return True

    @api.multi
    def upload_inventory_line_by_csv(self):
        lot_obj = self.env['stock.production.lot']
        product_obj = self.env['product.product']
        product_uom_obj = self.env['product.uom']
        inventory_line_obj = self.env['stock.inventory.line']
        inventory_obj = self.env['stock.inventory']

        file_data = str(base64.b64decode(self.file_data))

        row_cnt = 0
        #        try:
        for row_data in file_data.split(self.line_seprater):
            split_data = row_data.split(',')
            row_cnt += 1
            if row_cnt == 1:
                continue
            if len(split_data) == 3:
                product_code = split_data[0]
                product_qty = split_data[1]
                product_uom = split_data[2]

                product_id = product_obj.search([('default_code', '=', product_code)])
                if not len(product_id):
                    raise ValidationError(_("No product code found in database at row %s") % (row_cnt,))

                product_uom_id = product_uom_obj.search([('name', '=', product_uom)])
                if not len(product_uom_id):
                    raise ValidationError(_(
                        "No Product UOM %s found in database,UOM in file should be equal to unit of measure field, at row %s") % (
                                              product_uom, row_cnt,))

                stock_inventory_data = inventory_obj.browse(self.env.context['active_id'])
                location_id = stock_inventory_data.location_id.id
                inventory_line_obj.create(
                    {'inventory_id': self.env.context['active_id'], 'product_id': product_id[0].id,
                     'location_id': location_id, 'product_qty': product_qty, 'product_uom_id': product_uom_id[0].id})
        #            self.env.cr.commit()
        return True
